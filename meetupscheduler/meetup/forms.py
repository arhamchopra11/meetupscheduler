from __future__ import unicode_literals
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm

import json

from .models import EventList

# This form is used to register new users. This form has been extended from UserCreationForm class to include the email field along with the username and password. 
# We have also overridden the save method so that along with creating a new user the function will also initialize some structures like the EventList for each user.
class UserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
            e=EventList(user_id=user)
            EventList.initializeAvailableSlots(e)

        return user

# This form is used to add new events. It includes all the details of the event that are required
class EventForm(forms.Form): 
    subject = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'subject'}))
    detail  = forms.CharField(max_length=500, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'details'}))
    no_slots   = forms.IntegerField(initial = 1, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'no_slots'}))
 
# This form is used to Login users. It has asks for the username and password
class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30, widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password'}))

# This form is display all the different slots of a user
class UserResponseForm(forms.Form):
    choices = ()
    for j in range(48):
        if j%2 == 0:
            tmp = str(j/2) + (':00')
        else:
            tmp = str(j/2) + (':30')
        choices = choices + ((str(j),tmp),)
    slots = forms.MultipleChoiceField(choices = choices, widget= forms.CheckboxSelectMultiple())
    s=[]
    for i in range(len(choices)/4):
         s.append([choices[4*i], choices[4 * i + 1], choices[4 * i + 2], choices[4 * i + 3]])

    def clean_my_field(self):
        if(len(self.cleaned_data['slots'])==0):
            raise forms.ValidationError('Select atleast one field')
            return self.cleaned_data['slots']

    def choices_labels(self):
        s=[]
        for i in range(len(slots)/4):
             s.append([choices[4*i], choices[4 * i + 1], choices[4 * i + 2], choices[4 * i + 3]])
        return s

    def fn(self):
        return self.choices

# This form is also used to add new events. It also allows some selected users to be invited to an event by displaying the checklist of all users to invite
class SelectUser(forms.Form):
    subject = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'no_slots'}))
    detail  = forms.CharField(max_length=500, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'no_slots'}))
    no_slots   = forms.IntegerField(initial = 1, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'no_slots'}))
    check = forms.ModelMultipleChoiceField(label = 'check', widget= forms.CheckboxSelectMultiple(), queryset = User.objects.all())
