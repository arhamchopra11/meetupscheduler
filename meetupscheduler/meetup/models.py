from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models 
from django import forms
from django.core.validators import MaxValueValidator 
import json
# Create your models here.

class EventList(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)

    acceptEventList = models.CharField(max_length=5000,default='[]')
    pendEventList = models.CharField(max_length=5000,default='[]')
    availableSlotsList = models.CharField(max_length = 500, default = '[]')

    def __str__(self):
        return self.acceptEventList; 
    def getEventList(self, Listname):
        if Listname == "accept":
            return json.loads(self.acceptEventList)
        if Listname == "pending":
            return json.loads(self.pendEventList)
        if Listname == "availableSlots":
            return json.loads(self.availableSlotsList)

    def setEventList(self, event_list, Listname):
        if Listname == "accept":
            self.acceptEventList = json.dumps(event_list);
            self.save();
        elif Listname == "pending":
            self.pendEventList = json.dumps(event_list);
            self.save();
        elif Listname == "availableSlots":
            self.availableSlotsList = json.dumps(event_list);
            self.save();
        else:
            print "Not Working"

    def initializeAvailableSlots(self):
        l = [1 for i in range(48)]
        self.availableSlotsList = json.dumps(l)
        self.save()

class Event(models.Model):
    subject = models.CharField(max_length=50)
    detail = models.CharField(max_length=500)
    no_slots = models.IntegerField(default=1)
    NoUsersAccepted = models.IntegerField(default = 0)
    TotalUsers = models.IntegerField(default=0)
    AcceptedPerSlotList = models.CharField(max_length=500, default = [])
    is_scheduled = models.BooleanField(default = False)
    scheduled_at = models.IntegerField(default = -1,validators=[MaxValueValidator(48)])
    acceptEventList = models.CharField(max_length=5000,default='[]')
    pendEventList = models.CharField(max_length=5000,default='[]')

    def getAcceptedPerSlotList(self):
        return json.loads(self.AcceptedPerSlotList)

    def setAcceptedPerSlotList(self, user_list):
        self.AcceptedPerSlotList = json.dumps(user_list);
        self.save();

    def updateUsers(self, type):
        if type == "accept":
            self.NoUsersAccepted+=1
        else:
            self.NoUsersAccepted-=1
        self.save()

    def schedule(self, acceptUserList):
        l = json.loads(self.AcceptedPerSlotList)
        for i in range(len(l) - self.no_slots+1):
            if self.TotalUsers == int(l[i]):
                flag = True
                for j in acceptUserList:
                    u = EventList.objects.get(user_id = int(j[0]))
                    availableSlotsList = EventList.getEventList(u, "availableSlots")
                    for temp in range(i, i+self.no_slots):
                        if availableSlotsList[temp] == 0:
                            flag = False
                            break
                if flag:
                    self.is_scheduled = True
                    self.scheduled_at = i
                    self.save()
                    for j in acceptUserList:
                        u = EventList.objects.get(user_id = int(j[0]))
                        availableSlotsList = EventList.getEventList(u, "availableSlots")
                        for temp in range(i, i+self.no_slots):
                            availableSlotsList[temp] = 0
                        EventList.setEventList(u,availableSlotsList, "availableSlots")
                    break

    def discard(self, acceptUserList):
        for i in acceptUserList:
            u = EventList.objects.get(user_id = int(i[0]))
            availableSlotsList = EventList.getEventList(u, "availableSlots")
            for temp in range(self.scheduled_at, self.scheduled_at+self.no_slots):
                availableSlotsList[temp] = 1
            EventList.setEventList(u, availableSlotsList, "availableSlots")
        self.is_scheduled = False
        self.scheduled_at = -1
        self.save()

class UserList(models.Model):

    event_id = models.ForeignKey(Event, on_delete=models.CASCADE)
    acceptUserList = models.TextField(default='[]')
    pendUserList = models.CharField(max_length=500,default='[]')
    def __str__(self):
        return self.acceptUserList;



    def getUserList(self, Listname):
        if Listname == "accept":
            return json.loads(self.acceptUserList)
        if Listname == "pending":
            return json.loads(self.pendUserList)



    def setUserList(self, user_list, Listname):
        if Listname == "accept":
            self.acceptUserList = json.dumps(user_list);
            self.save();
        elif Listname == "pending":
            self.pendUserList = json.dumps(user_list);
            self.save();

        else:
            print "Not Working"
