from django import template

register = template.Library()

@register.filter(name='zip')
def zip_lists(a, b):
  return zip(a, b)

@register.filter(name='filterZero')
def filterZero(a):
    b=[];
    for i in range(len(a)):
        if not a[i]:
            print("NO")
        else:
            b.append(i);
    return b;

@register.filter(name='slotToTime')
def slotToTime(a):
	if(int(a)%2 == 0):
		return str(int(a)/2)+':00'
	else:
		return str((int(a)-1)/2) + ':30'
