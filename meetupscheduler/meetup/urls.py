from django.conf.urls import url
from . import views
urlpatterns = [
            url(r'^$',views.home,name='home'),
            url(r'^(?P<event_id>[0-9]+)/details/$',views.eventDetail, name='eventdetails'),
            url(r'^(?P<event_id>[0-9]+)/accept/$', views.accept, name='accept'),
            url(r'^(?P<event_id>[0-9]+)/reject/$', views.reject, name='reject'),
            url(r'^addEvent/$', views.addEvent, name='addEvent'),
            url(r'^invite/$', views.invite, name='invite'),
        ]
