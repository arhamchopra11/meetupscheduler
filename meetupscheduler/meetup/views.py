from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from  .models import EventList, Event, UserList
from .forms import EventForm, UserResponseForm, SelectUser

# Create your views here.

@login_required(login_url="login/")
# This function renders the home page. It is linked with the url- /home and / and it extract the event information of the user and provides the data to the webpage. 
def home(request):
    username = request.user.username;
    acceptlist=[];
    pendlist=[];
    acceptEventList=[];
    pendEventList=[];
    availableSlotsList=[]
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        user = None
    try:
        e = EventList.objects.get(user_id=user)
        acceptlist = EventList.getEventList(e,"accept");
        pendlist = EventList.getEventList(e,"pending");
        availableSlotsList = EventList.getEventList(e,"availableSlots")
    except EventList.DoesNotExist:
        acceptlist=[];
        pendlist=[];

    for id in acceptlist:
        a = Event.objects.get(pk=id);
        acceptEventList.append(a)
    for id in pendlist:
        a = Event.objects.get(pk=id);
        pendEventList.append(a)

    return render(request,'meetup/home.html',{'username':username,'acceptlist':acceptEventList,'pendlist':pendEventList, 'availableSlotsList':availableSlotsList})

# This function reders the eventDetail page for an event. It is linked with the url- /"event_id"/details and using the event_id provided it extracts all information of the event and sends it to the webpage.
def eventDetail(request,event_id):
    event = Event.objects.get(pk=event_id);
    username = request.user.username;
    user = User.objects.get(username=username)
    e = EventList.objects.get(user_id=user)
    acceptlist = EventList.getEventList(e,"accept");
    ul = UserList.objects.get(event_id=event)
    acceptUserList = UserList.getUserList(ul,"accept")
    a = [i[0] for i in acceptUserList]
    listAcceptedSlots = []
    if user.id in a:
        ind = a.index(user.id)
        listAcceptedSlots = acceptUserList[ind][1]
    if event.no_slots % 2 == 0:
        duration = str(event.no_slots/2) + ' hours'
    elif event.no_slots/2 != 0:
        duration = str((event.no_slots-1)/2) + ' hours 30 minutes'
    else:
        duration = '30 minutes'

    form = UserResponseForm();
    if int(event_id) in acceptlist:
        event_type='accept'
    else:
        event_type='pending'
    lis = Event.getAcceptedPerSlotList(event)
    return render(request,'meetup/eventdetails.html', {'event':event, 'event_type':event_type, 'form':form, 'duration':duration,'lis':lis,'listAcceptedSlots':listAcceptedSlots}) 

# This function is called when a form is submitted from the eventdetails by a user. The form contains the time slots when the user request the event to be scheduled. This function extracts the slots from request and add the slots to the list of slots accepted by all users, once all the users have requested their slots it computes a common timeslot of sufficient length matching with everybodys requests.  It sets certains fields in the event model as per the results of the scheduling. It also updates the list of events accepted by each user.
def accept(request, event_id):
    error_msg=""
    if request.method == 'GET':
        form = UserResponseForm();
        return render(request,'meetup/eventdetails.html',{'err_msg':"GET Methods are not Allowed"})
    if request.method == 'POST':
        form = UserResponseForm(request.POST)

        if not form.is_valid():
            err_msg="Please add atleast one slot"
        else:
            data = form.cleaned_data['slots']
            event = Event.objects.get(pk=event_id);
            Event.updateUsers(event, "accept")
            username = request.user.username;
            user = User.objects.get(username=username)
            el = EventList.objects.get(user_id=user)
            acceptlist = EventList.getEventList(el,"accept")
            pendlist = EventList.getEventList(el,"pending")
            acceptlist.append(event.id)
            pendlist.remove(event.id)
            EventList.setEventList(el,pendlist,'pending')
            EventList.setEventList(el,acceptlist,'accept')
            ul = UserList.objects.get(event_id=event)
            acceptUserList = UserList.getUserList(ul, 'accept')
            pendUserList = UserList.getUserList(ul,'pending')
            sl  = Event.getAcceptedPerSlotList(event)
            for i in data:
                sl[int(i)]+=1
            Event.setAcceptedPerSlotList(event,sl)
            c = [user.id, data]
            acceptUserList.append(c)
            pendUserList.remove(user.id)
            UserList.setUserList(ul, acceptUserList, 'accept')
            UserList.setUserList(ul, pendUserList, 'pending')
            if event.NoUsersAccepted == event.TotalUsers:
                Event.schedule(event, acceptUserList)
    return HttpResponseRedirect(reverse('home'))



# This function is called when an accept form is submitted from the eventdetails by a user. The function updates the EvenList of the user and Event itself accordingly by removing all data corresponding to the user. It sets certains fields in the event model like unscheduling the event if it was previously scheduled. It also updates the list of events accepted by each user.
def reject(request, event_id):
    acceptlist=[];
    pendlist=[];
    acceptEventList=[];
    pendEventList=[];

    event = Event.objects.get(pk=event_id);
    Event.updateUsers(event, "reject")

    username = request.user.username;
    user = User.objects.get(username=username)
    el = EventList.objects.get(user_id=user)
    acceptlist = EventList.getEventList(el,"accept")
    pendlist = EventList.getEventList(el,"pending")
    acceptlist.remove(event.id)
    pendlist.append(event.id)
    EventList.setEventList(el,pendlist,'pending')
    EventList.setEventList(el,acceptlist,'accept')
    ul = UserList.objects.get(event_id=event)
    acceptUserList = UserList.getUserList(ul, 'accept')
    pendUserList = UserList.getUserList(ul,'pending')

    if event.is_scheduled:
        Event.discard(event, acceptUserList)

    ind = [i[0] for i in acceptUserList].index(user.id)
    sl  = Event.getAcceptedPerSlotList(event)
    for i in acceptUserList[ind][1]:
        sl[int(i)]-=1
    Event.setAcceptedPerSlotList(event,sl)
    del acceptUserList[ind]
    pendUserList.append(user.id)
    UserList.setUserList(ul, acceptUserList, 'accept')
    UserList.setUserList(ul, pendUserList, 'pending')

    for id in acceptlist:
        a = Event.objects.get(pk=id);
        acceptEventList.append(a)
    for id in pendlist:
        a = Event.objects.get(pk=id);
        pendEventList.append(a)


    return HttpResponseRedirect(reverse('home'))



# This function reders the addEvent page for an event. It is linked with the url- /addEvent it initially sends an empty form. When a user adds an event this function is called and it extract the details from the request and sets up a new event and initialises all the necessary structures. Updates the pending list of users
def addEvent(request): 
    if request.method == 'GET':
        form = EventForm()

    else:
        # A POST request: Handle Form Upload
        form = EventForm(request.POST) # Bind data from request.POST into a PostForm

        # If data is valid, proceeds to create a new post and redirect the user
        if form.is_valid():
            users=User.objects.all()
            subject = form.cleaned_data['subject']
            detail = form.cleaned_data['detail']
            no_slots = form.cleaned_data['no_slots']
            TotalUsers = len(users)
            event = Event(subject=subject,detail=detail,no_slots=no_slots,TotalUsers=TotalUsers)
            event.save()
            AcceptedPerSlotList = [0 for i in range(48)]
            Event.setAcceptedPerSlotList(event, AcceptedPerSlotList)
            e=UserList(event_id=event)
            e.save()
            pendUserList = UserList.getUserList(e,'pending')
            for user in users:
                evlist=EventList.objects.get(user_id=user)
                pendlist = EventList.getEventList(evlist,'pending')
                pendlist.append(event.id)
                EventList.setEventList(evlist,pendlist,'pending')
                pendUserList.append(user.id)
            UserList.setUserList(e,pendUserList,'pending')
            return HttpResponseRedirect(reverse('home'))

    return render(request, 'meetup/addEvent.html', {'form': form})

# This function reders the invite page for an event. It is linked with the url- /invite it initially sends an empty form. When a user adds an event this function is called and it extract the details from the request and sets up a new event and initialises all the necessary structures. Updates the pending list of users. Very similar to addEvent but with the extra invitees field in the requests.
def invite(request): 
   ctr = 0
   if request.method == 'GET':
        form = SelectUser()
        return render(request,'meetup/eventdetails.html',{'err_msg':"GET Methods are not Allowed"})

   else:
       # A POST request: Handle Form Upload
       form = SelectUser(request.POST) # Bind data from request.POST into a PostForm

       # If data is valid, proceeds to create a new post and redirect the user
       if form.is_valid():
           users=User.objects.all()
           subject = form.cleaned_data['subject']
           detail = form.cleaned_data['detail']
           no_slots = form.cleaned_data['no_slots']
           invitees = form.cleaned_data['check']
           TotalUsers = len(invitees)
           event = Event(subject=subject,detail=detail,no_slots=no_slots,TotalUsers=TotalUsers)
           event.save()
           AcceptedPerSlotList = [0 for i in range(48)]
           Event.setAcceptedPerSlotList(event, AcceptedPerSlotList)
           e=UserList(event_id=event)
           e.save()
           pendUserList = UserList.getUserList(e,'pending')
           for i in invitees:
               evlist=EventList.objects.get(user_id=i)
               pendlist = EventList.getEventList(evlist,'pending')
               pendlist.append(event.id)
               EventList.setEventList(evlist,pendlist,'pending')
               pendUserList.append(i.id)
           UserList.setUserList(e,pendUserList,'pending')
           return HttpResponseRedirect(reverse('home'))

   return render(request, 'meetup/invite.html', {'form': form})


