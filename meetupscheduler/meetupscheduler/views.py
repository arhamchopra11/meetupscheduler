from django.shortcuts import render_to_response,redirect,render
from django.http import HttpResponseRedirect
from meetup.forms import UserCreationForm 

def register(request):
     if request.method == 'POST':
         form = UserCreationForm(request.POST)
         if form.is_valid():
             form.save()
             return redirect(registration_complete)

     else:
         form = UserCreationForm()
     token = {}
     token['form'] = form

     return render(request,'registration/register.html', {'form':form})

def registration_complete(request):
     return render(request,'registration/registration_complete.html')
